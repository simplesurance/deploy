{{ if eq .Values.kind "statefulset" }}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ template "app.fullname" . }}
  labels: {{- include "app.labels" . | nindent 4 }}
spec:
  selector:
    matchLabels: {{- include "app.matchLabels" . | nindent 6 }}
  replicas: {{ .Values.replicas }}
  podManagementPolicy: {{ .Values.podManagementPolicy }}
  updateStrategy:
    type: {{ .Values.updateStrategy }}
  {{- if .Values.persistence.enabled }}
  volumeClaimTemplates:
  - metadata:
      name: {{ template "app.fullname" . }}
    {{- if .Values.persistence.labels.enabled }}
      labels: {{- include "app.labels" . | nindent 8 }}
        {{- range $key, $value := .Values.labels }}
        {{ $key }}: {{ $value | quote }}
        {{- end }}
    {{- end }}
    {{- with .Values.persistence.annotations  }}
      annotations:
{{ toYaml . | indent 8 }}
    {{- end }}
    spec:
{{ toYaml .Values.volumeClaimTemplate | indent 6 }}
  {{- end }}
  template:
    metadata:
      labels: {{- include "app.labels" . | nindent 8 }}
      annotations:
        rollme: {{ randAlphaNum 5 | quote }}
        checksum/config: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
      {{- if .Values.image.annotations }}
      {{- toYaml .Values.image.annotations | nindent 8 }}
      {{- end }}
    spec:
      {{- if .Values.sa.enabled }}
      serviceAccountName: {{ template "app.fullname" . }}
      {{- end }}
      {{- if .Values.image.imagePullSecrets }}
      imagePullSecrets: {{ toYaml .Values.image.imagePullSecrets | nindent 8 }}
      {{- end }}
      {{- if .Values.affinity }}
      affinity: {{- include "app.tplValue" (dict "value" .Values.affinity "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.appSelector }}
      appSelector: {{- include "app.tplValue" (dict "value" .Values.appSelector "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.tolerations }}
      tolerations: {{- include "app.tplValue" (dict "value" .Values.tolerations "context" $) | nindent 8 }}
      {{- end }}
      containers:
        - name: {{ template "app.fullname" . }}
          image: {{ .Values.image.path }}:{{ .Values.image.tag }}
          imagePullPolicy: {{ .Values.image.pullPolicy | quote }}
          env:
          {{- if .Values.extraEnv }}
          {{- toYaml .Values.extraEnv | nindent 12 }}
          {{- end }}
          {{- if .Values.env }}
          {{- range $key, $val := .Values.env }}
            - name: {{ $key }}
              value: {{ $val | quote }}
          {{- end }}
          {{- end }}
          {{- if .Values.secrets }}
          {{- range $key, $val := .Values.secrets }}
            - name: {{ .env }}
              valueFrom:
                secretKeyRef:
                  name: {{ .name }}
                  key: {{ .key }}
          {{- end }}
          {{- end }}
          {{- if .Values.image.workingDir }}
          workingDir: {{ .Values.image.workingDir }}
          {{- end }}
          {{- if .Values.image.command }}
          command:
          {{- range .Values.image.command }}
            - {{ . }}
          {{- end }}
          {{- end }}
          {{- if .Values.image.args }}
          args:
          {{- range .Values.image.args }}
            - {{ . }}
          {{- end }}
          {{- end }}
          ports: {{- toYaml .Values.ports | nindent 12 }}
          {{- if .Values.livenessProbe }}
          livenessProbe: {{- toYaml .Values.livenessProbe | nindent 12 }}
          {{- end }}
          {{- if .Values.livenessProbe }}
          readinessProbe: {{- toYaml .Values.readinessProbe | nindent 12 }}
          {{- end }}
          {{- if .Values.resources }}
          resources: {{- toYaml .Values.resources | nindent 12 }}
          {{- end }}
          {{- if .Values.securityContext }}
          securityContext: {{- toYaml .Values.securityContext | nindent 12 }}
          {{- end }}
          {{- if .Values.volumeMounts }}
          volumeMounts: {{- toYaml .Values.volumeMounts | nindent 12 }}
          {{- end }}
      {{- if .Values.extraContainers }}
      # Currently some extra blocks accept strings
      # to continue with backwards compatibility this is being kept
      # whilst also allowing for yaml to be specified too.
      {{- if eq "string" (printf "%T" .Values.extraContainers) }}
{{ tpl .Values.extraContainers . | indent 8 }}
      {{- else }}
{{ toYaml .Values.extraContainers | indent 8 }}
      {{- end }}
      {{- end }}
      {{- if .Values.volumes }}
      volumes: {{- toYaml .Values.volumes | nindent 8 }}
      {{- end }}
{{- end }}